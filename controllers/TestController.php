<?php

namespace app\controllers;

use Yii;
use app\models\Register;

class TestController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	$message = 'Hello, my name is Developer';

        return $this->render('index', [
        	'message' => $message,
        ]);
    }

    public function actionRegister()
    {
        $model = new Register();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('register-seccess', 'success');
                $model = new Register();
                // return $this->render('register', ['model' => $model]);
            }
        }

        return $this->render('register', [
            'model' => $model
        ]);
    }

}
