<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<h1><?= 'Register Page' ?></h1>

<?php if (Yii::$app->session->hasFlash('register-seccess')): ?>

    <div class="alert alert-success">
        Seccessfully registered
    </div>

<?php else: ?>

    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['id' => 'register-form']); ?>

                <?= $form->field($model, 'full_name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'username')->textInput(['max'=>30]); ?>

                <?= $form->field($model, 'password')->passwordInput(['min'=>3]) ?>

                <?= $form->field($model, 'email')->textInput(['type'=>'email']); ?>

                <?= $form->field($model, 'phone')->textInput(['type'=>'number']); ?>

                <div class="form-group">
                    <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

<?php endif; ?>