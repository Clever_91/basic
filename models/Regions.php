<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

class Regions extends \yii\db\ActiveRecord
{
    public $multipleImages;
    public $_imageDelete;
    public $_imageMain;

    public static function tableName()
    {
        return 'tbl_regions';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'foxunion\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    public function rules()
    {
        return [
            [['title'], 'unique'],
            [['title'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['created_at', 'updated_at', '_imageDelete', '_imageMain'], 'safe'],
            [['title'], 'string', 'max' => 250],
            [['multipleImages'], 'file', 'maxFiles' => 5, 'extensions' => 'jpg png jpeg bmp'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название Региона',
            'description' => 'Комментарии к Региону',
            'status' => 'Status',
            'multipleImages' => 'Изображения',
            '_imageDelete' => 'Удалить изображение',
            '_imageMain' => 'Сделать главным',
            'created_at' => 'Времия Добавление',
            'updated_at' => 'Updated At',
        ];
    }

    public function getCities()
    {
        return $this->hasMany(Cities::className(), ['region_id' => 'id']);
    }

    public function getStreets()
    {
        return $this->hasMany(Streets::className(), ['region_id' => 'id']);
    }

    public function afterSave($insert, $values)
    {
        $this->imageDelete();
        $this->imageAdd();
        parent::afterSave($insert, $values);
    }

    private function imageAdd()
    {
        $this->multipleImages = UploadedFile::getInstances($this, 'multipleImages');
        if ($this->multipleImages && $this->validate()) {
            foreach ($this->multipleImages as $image) {
                $this->attachImage($image->tempName, true, $image->name, 0, $image->extension);
                $this->clearImagesCache();
                $image->reset();
            }
        }
    }

    private function imageDelete()
    {
        foreach ($this->getImages() as $img) {
            if(isset($this->_imageMain[$img->id]) && $this->_imageMain[$img->id]==1)
                $this->setMainImage($img);
            if(isset($this->_imageDelete[$img->id]) && $this->_imageDelete[$img->id]==1)
                $this->removeImage($img);
        }
    }
}
