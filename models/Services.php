<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use common\models\services\SphinxManager;
use common\behaviors\FavoritableBehavior;


class Services extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'foxunion\yii2images\behaviors\ImageBehave',
            ],
            'favoritable' => [
                'class' => FavoritableBehavior::className(),
            ],
            'common\behaviors\StatisticsBehavior',
        ];
    }

    public $_image;
    public $_deleteimage;

    public static function tableName()
    {
        return 'tbl_services';
    }

    public static function indexName()
    {
        return ['services_core', 'services_delta'];
    }

    public static function deltaIndexName()
    {
        return 'services_delta';
    }

    public static function searchableColumns()
    {
        return ['id', 'title'];
    }

    public function getCategoryAttributes()
    {
        return $this->hasMany(ServiceCategoryAttributes::className(), ['service_category_id' => 'id' ]);
    }

    public function rules()
    {
        return [
            [['id_category', 'title', 'publication_day'], 'required'],
            [['id_category', 'is_visible', 'is_favorite', 'is_compare', 'views_days', 'views', 'author_id', 'publication_day'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 255],
            ['dynamic_attributes', 'uniqueAndNotEmptyKeysJson'],
            [['description', 'dynamic_attributes', 'keyword', 'avg_price', 'id_unit', 'cost', 'title_example'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_category' => 'Категория',
            'title' => 'Заголовок',
            'title_example' => 'Пример заголовок объявления',
            'description' => "Описание",
            'dynamic_attributes' => "Динамические атрибуты",
            'keyword' => 'Теги для поиска',
            '_image' => 'Изображение',
            'is_visible' => 'Видимость',
            'views_days' => 'Накрутить просмотры за день',
            'views' => 'Накрутить просмотры',
            'is_favorite' => 'Популярная услуга',
            'is_compare' => 'Использовать в похожих услугах для этого раздела',
            'avg_price' => 'Стоимость от',
            'id_unit' => 'Единица измерения по умолчанию',
            'author_id' => 'Автор статьи',
            'cost' => 'Цена публикации (Рубль)',
            'publication_day' => 'Количество дней публикации',
        ];
    }

    public function uniqueAndNotEmptyKeysJson($attribute, $params)
    {
        $dynamicAttributes = is_array($this->dynamic_attributes)
            ? $this->dynamic_attributes['params']
            : Json::decode($this->dynamic_attributes)['params'];
        $hasDuplicateAttribute = false;
        $hasEmptyAttribute = false;
        $uniqueKeys = [];
        foreach ($dynamicAttributes as $key => $dynamicAttribute) {
            if (!in_array($dynamicAttribute['key'], $uniqueKeys)) {
                array_push($uniqueKeys, $dynamicAttribute['key']);
            } else {
                $hasDuplicateAttribute = true;
                break;
            }
            if ($dynamicAttribute['key'] == '' || $dynamicAttribute['value'] == '') {
                $hasEmptyAttribute = true;
                break;
            }
        }
        if ($hasEmptyAttribute OR $hasDuplicateAttribute) {
            $this->addError($attribute, 'Название атрибута не может быть пустым или повторяться. Значение атрибута не может быть пустым');
        }
    }

    public function getIndexData()
    {
        return array(
            'id' => (integer)$this->id,
            'title' => $this->title,
            'description' => $this->description,
        );
    }

    public function getSameServicesLinks()
    {
        return SameService::find()->where("first_id={$this->id} or second_id={$this->id}");
    }


    public function getSameServices($limit = 3)
    {
        $sameServicesIds = [];
        foreach ($this->getSameServicesLinks()->all() as $sameServicesLink) {
            if ($sameServicesLink->first_id == $this->id) {
                $sameServicesIds[] = $sameServicesLink->second_id;
            } else {
                $sameServicesIds[] = $sameServicesLink->first_id;
            }
        }
        return (count($sameServicesIds) > 0) ? Services::find()->where("id IN (" . implode(',', $sameServicesIds) . ")")->limit($limit) : [];
    }


    public function getCategory()
    {
        return $this->hasOne(ServicesCategory::className(), ['id' => 'id_category']);
    }

    public function getServicesDatas()
    {
        return $this->hasMany(ServicesData::className(), ['id_service' => 'id']);
    }

    public function getServicesSimilars()
    {
        return $this->hasMany(ServicesSimilar::className(), ['id_service' => 'id']);
    }

    public function offersFrom($userId)
    {
        return $this->hasOne(ServicesData::className(), ['id_service' => 'id'])
            ->from(ServicesData::tableName())
            ->where(['id_user' => $userId])
            ->all();
    }

    public function beforeSave($insert)
    {
        if (is_array($this->dynamic_attributes)) {
            $this->dynamic_attributes = Json::encode($this->dynamic_attributes);
        }
        $this->delta = true;
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            $this->deleteServiceFromUsers();
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $values)
    {

        $this->imageUpdate();
        $this->addServiceForUsers();
        SphinxManager::updateIndex(self::deltaIndexName());

        // save attributes
        $old_id = ServiceCategoryAttributes::find()->where(['service_category_id' => $this->id])->select('id')->column();
        $new_id  = [];

        $data = Yii::$app->request->post('ServiceCategoryAttributes');
        if($data)
        {
            foreach ($data['id'] as $key => $value) {

                if($data['id'][$key])
                    $model = ServiceCategoryAttributes::findOne($data['id'][$key]);
                else
                    $model = new ServiceCategoryAttributes();

                $model->name = $data['name'][$key];
                $model->type = $data['type'][$key];
                $model->values = $data['values'][$key];
                $model->service_category_id = $this->id;
                if($model->save())
                    $new_id[] = $model->id;
            }

            $to_delete_list = array_diff($old_id, $new_id);

            if($to_delete_list)
            {
                ServiceCategoryAttributes::deleteAll(['id' => $to_delete_list]);
                ServiceCategoryAttributesValue::deleteAll(['category_attributes_id' => $to_delete_list]);
            }

        }



        parent::afterSave($insert, $values);
    }

    private function imageUpdate()
    {
        $this->_image = UploadedFile::getInstance($this, '_image');
        if ($this->_image AND !empty($this->_image->tempName) AND $this->validate())
        {
            $this->removeImages();
            $this->clearImagesCache();
            $this->attachImage($this->_image->tempName, true, $this->_image->name, 0, $this->_image->extension);
            $this->clearImagesCache();
            $this->_image->reset();
        }
    }

    private function addServiceForUsers()
    { // Если новая услуга то добавить ко всем пользователям в сфере
        foreach ($this->category->Users as $key => $user) {
            $modelData = new ServicesData();
            $modelData->id_user = $user->id;
            $modelData->id_service = $this->id;
            $modelData->id_unit = $this->id_unit;
            $modelData->minvolume = '0';
            $modelData->inprice = '0';
            $modelData->save();
        }
    }

    private function deleteServiceFromUsers()
    { // Удаляем связанные обьекты.
        foreach ($this->servicesDatas as $key => $servicesData) {
            $servicesData->delete();
        }
    }

    public function getUsers(){
        $users = Users::find()->select(['id', 'email'])->all();
        return ArrayHelper::map($users, 'id', 'email');
    }

    public function getAuthor(){
        return $this->hasOne(Users::className(), ['id' => 'author_id']);
    }
}
