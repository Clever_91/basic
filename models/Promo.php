<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use foxunion\yii2images\behaviors\ImageBehave;
use common\models\services\SphinxManager;

/**
 * This is the model class for table "promos".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $related_class
 * @property string $view
 * @property integer $related_id
 * @property integer $is_visible
 */
class Promo extends \yii\db\ActiveRecord
{
    const VIEW_LANDSCAPE = 'landscape';
    const VIEW_PORTRAIT = 'portrait';

    public static $views = [
        self::VIEW_LANDSCAPE => 'Альбомный',
        self::VIEW_PORTRAIT => 'Портретный',
    ];

    public function behaviors()
    {
        return [
            'image' => [
                'class' => ImageBehave::className(),
            ]
        ];
    }
    
    public $image;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promos';
    }

    public static function indexName()
    {
        return ['promo_core', 'promo_delta'];
    }

    public static function deltaIndexName()
    {
        return 'promo_delta';
    }

    public static function searchableColumns()
    {
        return ['id', 'title', 'description'];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['related_class', 'related_id', 'title'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg png'],
            [['is_visible', 'related_id'], 'integer'],
            [['title', 'view'], 'string', 'max' => 128],
            ['view', 'match', 'pattern' => '/^(landscape|portrait)$/'],
            [['description', 'related_class'], 'string'],
            [['related_class', 'related_id'], 'unique', 'targetAttribute' => ['related_class', 'related_id']],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'image' => 'Фоновое изображение',
            'related_class' => 'Связанный класс',
            'related_id' => 'ID связанной записи',
            'view' => 'Вид',
            'is_visible' => 'Видимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne($this->related_class, ['related_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * alias getOwner()
     */
    public function getUser()
    {
        return $this->getOwner();
    }


    public function beforeSave($insert)
    {
        $this->delta = true;
        return (parent::beforeSave($insert));
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }
    
    public function afterSave($insert, $values)
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        if ($this->image !== null && $this->image->tempName !== '' && $this->image instanceof UploadedFile) {
            if (!$this->isNewRecord) {
                $this->removeImages();
            }
            $this->attachImage($this->image->tempName);
            $this->clearImagesCache();
            $this->image->reset();
        }
        SphinxManager::updateIndex(self::deltaIndexName());
        parent::afterSave($insert, $values);
    }
    
}
