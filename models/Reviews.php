<?php

namespace common\models;

use common\helpers\SmsHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\UploadedFile;

use vova07\fileapi\behaviors\UploadBehavior;


/**
 * This is the model class for table "tbl_reviews".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $service_name
 * @property string $text
 * @property integer $is_visible
 * @property integer $is_moderated
 * @property datetime $created_at
 * @property datetime $updated_at
 *
 * @property TbLReviewsImagesHasTblReviews $tbLReviewsImagesHasTblReviews
 * @property TblReviewsImages[] $tbLReviewsImages
 * @property TblUsers $idUser
 * @property TblReviewsImageRelations[] $tblReviewsImageRelations
 * @property TblReviewsRatingRelations[] $tblReviewsRatingRelations
 */
class Reviews extends \yii\db\ActiveRecord
{
    public $multipleImages;
    public $_imageDelete;
    public $_imageMain;

    public function behaviors()
    {
        return [
            'images' => [
                'class' => 'foxunion\yii2images\behaviors\ImageBehave',
            ],
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'text', 'sender'], 'required'],
            [['id_user', 'id_rating', 'is_visible'], 'integer'],
            [['text', 'service_name'], 'string'],
            [['_imageDelete', '_imageMain', 'sender', "id_rating", 'id_service'], 'safe'],
            [['multipleImages'], 'file', 'maxFiles' => 5, 'extensions' => 'jpg png jpeg bmp'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'service_name' => 'Услуга',
            'sender' => 'Отправитель',
            'text' => 'Сообщение',
            'is_visible' => 'Видимость',
            'is_moderated' => 'Проверено модератором',
            'updated_at' => 'Изменен',
            'created_at' => 'Добавлено',
            'multipleImages' => 'Изображения',
            '_imageDelete' => 'Удалить изображение',
            '_imageMain' => 'Сделать главным',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRating()
    {
        return $this->hasOne(ReviewsRating::className(), ['id' => 'id_rating']);
    }

    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewsRatingRelations()
    {
        return $this->hasMany(ReviewsRatingRelations::className(), ['id_review' => 'id']);
    }

    public function afterSave($insert, $values)
    {
        $this->imageDelete();
        $this->imageAdd();

        $phone = $this->user->usersProfile->smsPhone;
        $phone = str_replace(['*', '+', '-', '(', ')'], '', $phone);

        if($insert)
        {
            SmsHelper::sendSms($phone, 'Вам (вашей компании) добавлен отзыв. Можете его прочитать в личном кабинете.');
        }
        return (parent::afterSave($insert, $values));
    }

    private function imageAdd()
    {
        $this->multipleImages = UploadedFile::getInstances($this, 'multipleImages');
        if ($this->multipleImages && $this->validate()) {
            foreach ($this->multipleImages as $image) {
                $this->attachImage($image->tempName);
                $this->clearImagesCache();
            }
        }
    }
    private function imageDelete()
    {
        foreach ($this->getImages() as $img) {
            if(isset($this->_imageMain[$img->id]) && $this->_imageMain[$img->id]==1)
                $this->setMainImage($img);
            if(isset($this->_imageDelete[$img->id]) && $this->_imageDelete[$img->id]==1)
                $this->removeImage($img);
        }
    }
}
