<?php

namespace common\models;

use Yii;
use yii\db\Expression;
use yii\web\UploadedFile;
use yii\behaviors\TimestampBehavior;
use common\models\queries\PersonalMessageQuery;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property string $subject
 * @property string $body
 * @property string $type
 */
class PersonalMessage extends \yii\db\ActiveRecord
{
  const STATUS_NEW = 0;
  const STATUS_READ = 1;
  const STATUS_TRASHED = 2;

  const TYPE_INBOX = 'inbox';
  const TYPE_SENT = 'sent';
  const TYPE_SYSTEM = 'system';

  public $images;

  public static function find()
  {
    return new PersonalMessageQuery(get_called_class());
  }

  public static function tableName()
  {
    return 'personal_messages';
  }

  public function behaviors()
  {
    return [
      'images' => [
        'class' => 'foxunion\yii2images\behaviors\ImageBehave',
      ],
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'created_at',
        'updatedAtAttribute' => 'updated_at',
        'value' => new Expression('NOW()'),
      ],
    ];
  }

  public function rules()
  {
    return [
      [['user_id', 'body'], 'required'],
      [['status', 'user_id'], 'integer'],
      [['type'], 'match', 'pattern' => '/^(inbox|sent|system)$/'],
      [['body'], 'string'],
      [['subject', 'type'], 'string', 'max' => 255],
      [['images'], 'safe']
    ];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'subject' => 'Тема',
      'body' => 'Сообщение',
      'type' => 'Тип',
      'status' => 'Статус',
      'user_id' => 'Пользователь',
      'created_at' => 'Получено',
      'updated_at' => 'Прочитано',
    ];
  }

  public function getUser()
  {
    return $this->hasOne(Users::className(), ['id' => 'user_id']);
  }

  public function isNew()
  {
    return ($this->status == self::STATUS_NEW);
  }

  public function isSystem()
  {
    return ($this->type == self::TYPE_SYSTEM);
  }

  public function isTrashed()
  {
    return ($this->status == self::STATUS_TRASHED);
  }

  public function read()
  {
    $this->status = self::STATUS_READ;
    return $this->update();
  }

  public function getUserProfile()
  {
    return $this->hasOne(UsersProfile::className(), ['id_user' => 'user_id']);
  }


  public function trash()
  {
    $this->status = self::STATUS_TRASHED;
    return $this->update();
  }

  public function afterSave($insert, $values)
  {
    $this->images = UploadedFile::getInstance($this, 'images');
    if ($this->images !== null && $this->images->tempName !== '' && $this->images instanceof UploadedFile) {
      $this->attachImage($this->images->tempName);
      $this->images->reset();
    }
    return (parent::afterSave($insert, $values));
  }
}
