<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use common\behaviors\FavoritableBehavior;
use common\models\services\SphinxManager;

class ServicesCategory extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'foxunion\yii2images\behaviors\ImageBehave',
            ],
            'favoritable' => [
                'class' => FavoritableBehavior::className(),
            ],
            'common\behaviors\StatisticsBehavior',
        ];
    }
    public $real_estate=1;
    public $core_services=2;
    public $_image;
    public $_deleteimage;

    public static function tableName()
    {
        return 'tbl_services_category';
    }

    public static function indexName()
    {
        return ['services_category_core', 'services_category_delta'];
    }

    public static function deltaIndexName()
    {
        return 'services_category_delta';
    }

    public static function searchableColumns()
    {
        return ['id', 'title', 'keyword'];
    }

    public function rules()
    {
        return [
            [['parent_id', 'position', 'is_visible','_order', 'type'], 'integer'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 128],
            ['dynamic_attributes', 'uniqueAndNotEmptyKeysJson'],
            [['keyword', 'no_sphere_text'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'title' => 'Заголовок',
            'keyword' => 'Теги для поиска',
            '_image' => 'Изображение',
            'position' => 'Позиция',
            'is_visible' => 'Видимость',
            '_deleteimage' => 'Удалить изображение',
            '_order' => 'Сортировка',
            'no_sphere_text' => 'Текст при отсутствии предложений',
            'type'=>'тип',
        ];
    }

    public function uniqueAndNotEmptyKeysJson($attribute, $params)
    {
        $dynamicAttributes = is_array($this->dynamic_attributes)
            ? $this->dynamic_attributes['params']
            : Json::decode($this->dynamic_attributes)['params'];
        $hasDuplicateAttribute = false;
        $hasEmptyAttribute = false;
        $uniqueKeys = [];
        foreach ($dynamicAttributes as $key => $dynamicAttribute) {
            if (!in_array($dynamicAttribute['key'], $uniqueKeys)) {
                array_push($uniqueKeys, $dynamicAttribute['key']);
            } else {
                $hasDuplicateAttribute = true;
                break;
            }
            if ($dynamicAttribute['key'] == '' || $dynamicAttribute['value'] == '') {
                $hasEmptyAttribute = true;
                break;
            }
        }
        if ($hasEmptyAttribute OR $hasDuplicateAttribute) {
            $this->addError($attribute, 'Название атрибута не может быть пустым или повторяться. Значение атрибута не может быть пустым');
        }
    }

    public function getServices()
    {
        return $this->hasMany(Services::className(), ['id_category' => 'id']);
    }

    public function getAllNestedServices()
    {
        return Services::find()->where(['id_category' => $this->nestedIds])->all();
    }

    protected function getNestedIds()
    {
        $ids = [$this->id];
        foreach ($this->servicesCategories as $childCategory) {
            $ids[] = $childCategory->id;
            if ($childCategory->servicesCategories) {
                array_merge($ids, $childCategory->nestedIds);
            }
        }
        return $ids;
    }

    public function getParent()
    {
        return $this->hasOne(ServicesCategory::className(), ['id' => 'parent_id']);
    }

    public function applyAttributesToOwnedServices()
    {
        try {
            $command = self::getDb()->createCommand();
            $command->update(Services::tableName(), ['dynamic_attributes' => $this->dynamic_attributes], ['id_category' => $this->id]);
            $command->execute();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getParentsLine()
    {
        $parentsLine = [];
        $parent = $this->parent;
        while ($parent) {
            array_push($parentsLine, $parent);
            $parent = $parent->parent;
        }
        return $parentsLine;

    }

    public function getServicesCategories()
    {
        $where = [];
         $user =Yii::$app->user->identity;
       
        if (Yii::$app->id == "app-frontend" AND $user) {
            $where= ['type' => $user->user_type];
        }
        return $this->hasMany(ServicesCategory::className(), ['parent_id' => 'id' ])->orderBy(['_order' => SORT_ASC])->where($where);
    }

    public function getTblServicesImages()
    {
        return $this->hasMany(TblServicesImages::className(), ['id_service' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

    public function getFullTitle()
    {
        return $this->NameRecursive(null, $this);
    }

    private function NameRecursive($str, $model)
    {
        if ($str)
            $name = $model->title . " > " . $str;
        else
            $name = $model->title;
        if ($model->parent)
            return $this->NameRecursive($name, $model->parent);
        else
            return $name;
    }

    public function getUserServiceCategory()
    {
        return $this->hasMany(UserServiceCategories::className(), ['id_service_category' => 'id']);
    }

    public function getHeadfields()
    {
        return $this->hasMany(Dinamicfields::className(), ['service_category_id' => 'id'])
            ->andOnCondition(['parent_id' => 0, 'position' => 'head']);
    }

    public function getMainfields()
    {
        return $this->hasMany(Dinamicfields::className(), ['service_category_id' => 'id'])
            ->andOnCondition(['parent_id' => 0, 'position' => 'main']);
    }

    public function getImportantfields()
    {
        return $this->hasMany(Dinamicfields::className(), ['service_category_id' => 'id'])
            ->andOnCondition(['parent_id' => 0, 'position' => 'important']);
    }

    public function getAdditionalfields()
    {
        return $this->hasMany(Dinamicfields::className(), ['service_category_id' => 'id'])
            ->andOnCondition(['parent_id' => 0, 'position' => 'additional']);
    }

    public function getUsers()
    {
        return Users::find()->joinWith('usersServicesCategories')->where(["tbl_users_services_categories.id_service_category" => $this->id])->all();
    }

    public function beforeSave($insert)
    {
        if (is_array($this->dynamic_attributes)) {
            $this->dynamic_attributes = Json::encode($this->dynamic_attributes);
        }
        $this->delta = true;
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $values)
    {
        $this->imageUpdate();
        SphinxManager::updateIndex(self::deltaIndexName());
        parent::afterSave($insert, $values);
    }
    public function imageUpdate()
    {
        $this->_image = UploadedFile::getInstance($this, '_image');
        if ($this->_image AND !empty($this->_image->tempName) AND $this->validate())
        {
            $this->removeImages();
            $this->clearImagesCache();
            $this->attachImage($this->_image->tempName, true, $this->_image->name, 0, $this->_image->extension);
            $this->clearImagesCache();
            $this->_image->reset();
        }
        if ($this->_deleteimage) {
            $this->removeImages();
            $this->clearImagesCache();
        }
    }
    
    public static function dropdownList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }
}
