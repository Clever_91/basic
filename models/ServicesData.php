<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\web\UploadedFile;
use common\models\services\SphinxManager;

class ServicesData extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'foxunion\yii2images\behaviors\ImageBehave',
            ],
            'common\behaviors\StatisticsBehavior',
        ];
    }
    
    public $_image;
    public $_deleteimage;
    public $l_image;
    public $p_image;

    public static function tableName()
    {
        return 'tbl_services_data';
    }

    public static function indexName()
    {
        return ['services_data_core', 'services_data_delta'];
    }

    public static function deltaIndexName()
    {
        return 'services_data_delta';
    }

    public static function searchableColumns()
    {
        return ['id', 'discount', 'avg_price', 'average_check', 'is_qualitymark', 'recommendation', 'city', 'area', 'is_active', 'online_appointment', 'dynamic_attributes', 'description'];
    }

    public function getCategoryAttributes()
    {
        return $this->hasMany(ServiceCategoryAttributes::className(), ['service_category_id' => 'id_service']);
    }

    public function getCategoryAttributesValue()
    {
        return $this->hasMany(ServiceCategoryAttributesValue::className(), ['service_id' => 'id']);
    }

    public function rules()
    {
        return [
            // [['id_service', 'id_user'], 'required'],
            [
                ['is_compare', 'is_popular', 'id_service', 'id_user', 'id_unit', 'discount', 'delta', 'is_sponsored', 'is_qualitymark', 'is_active', 'visible', 'online_appointment', 'price_type', 'sticker_id'],
                'integer'
            ],
            [
                ['title', 'avg_price', 'old_price', 'description', 'average_check', 'dynamic_attributes', 'recommendation', 'worktime', '_deleteimage', '_image', 'l_image', 'position', 'likely_spheres', 'added', 'publication_date', 'created_at', 'updated_at', 'map_lon', 'map_lat'],
                'safe'
            ],
            [
                'dynamic_attributes', 
                'uniqueAndNotEmptyKeysJson'
            ],
            [
                ['avg_price', 'price', 'price_doc', 'average_check', 'price_header'],
                'string', 
                'max' => 100
            ],
            [
                ['map_content'],
                'string', 
                'max' => 300
            ],
            [
                ['minvolume', 'inprice'], 
                'default'
            ],
            [
                'price',
                'file',
                'extensions' => 'doc, docx, pdf, xml, txt, odt',
                'on' => ['insert', 'update']
            ],
            [
                ['_image', 'l_image', 'p_image'],
                'file',
                'extensions' => 'png, jpg',
                'maxSize' => 1024 * 500
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Загаловок',
            'id_service' => 'Сфера',
            'sticker_id' => 'Стиккер',
            'id_user' => 'Пользователь',
            'id_unit' => 'Величина измерения',
            'avg_price' => 'Цена от',
            'price' => 'Цена',
            'old_price' => 'Старая цена',
            '_image' => 'Изображение',
            'worktime' => 'Время работы',
            'description' => 'Описание',
            'discount' => 'Скидка',
            'is_qualitymark' => 'Знак качества',
            'recommendation' => 'Лейбл',
            '_deleteimage' => 'Удалить изображение',
            'is_active' => 'Включено',
            'visible' => 'Отображать',
            'online_appointment' => 'Онлайн запись',
            'dynamic_attributes' => 'Динамические атрибуты',
            'is_sponsored' => 'Оплаченная',
            'is_popular' => 'Использовать как популярная',
            'is_compare' => 'Использовать в похожих',
            'minvolume' => 'мин. объем в количестве',
            'inprice' => 'в цене',
            'l_image' => 'похожих Изображение',
            'p_image' => 'популярных',
            'likely_spheres' => 'похожих Uslugi',
            'price_type' => 'Тип прайса',
            'price_header' => 'Загаловок прайса',
            'publication_date' => 'Дата публикации',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Дата изменения',
            'map_content' => 'Адрес',
            'map_lon' => 'Долгота',
            'map_lat' => 'Широта',
        ];
    }

    public function uniqueAndNotEmptyKeysJson($attribute, $params)
    {

        $dynamicAttributes = is_array($this->dynamic_attributes)
            ? $this->dynamic_attributes
            : Json::decode($this->dynamic_attributes);
        $hasDuplicateAttribute = false;
        $hasEmptyAttribute = false;
        $uniqueKeys = [];
        foreach ($dynamicAttributes as $key => $dynamicAttribute) {
            if (!in_array($dynamicAttribute, $uniqueKeys)) {
                array_push($uniqueKeys, $dynamicAttribute);
            } else {
                $hasDuplicateAttribute = true;
                break;
            }
            if ($dynamicAttribute == '') {
                $hasEmptyAttribute = true;
                break;
            }
        }
        if ($hasEmptyAttribute OR $hasDuplicateAttribute) {
            $this->addError($attribute, 'Название атрибута не может быть пустым или повторяться.');
        }
    }

    public function getIndexData()
    {
        return array(
            'id' => (integer)$this->id,
            'avg_price' => (integer)$this->avg_price,
            'description' => $this->description,
        );
    }

    public function getViewAvgPrice()
    {
        if ($this->avg_price == "")
            return $this->service->avg_price;
        else
            return $this->avg_price;
    }

    public function getViewDescription()
    {
        if ($this->description == "")
            return $this->service->description;
        else
            return $this->description;
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    public function getUserProfile()
    {
        return $this->hasOne(UsersProfile::className(), ['id_user' => 'id'])->via('user');
    }

    public function getAddress()
    {
        return $this->hasOne(UsersAddress::className(), ['id_user' => 'id_user']);
    }

    public function getChargeableServices()
    {
        return ChargeableService::find()->where(['class_name' => self::className()]);
    }

    public function getService()
    {
        return $this->hasOne(Services::className(), ['id' => 'id_service']);
    }

    public function getUnit()
    {
        return $this->hasOne(ServicesUnits::className(), ['id' => 'id_unit']);
    }

    public function getSticker()
    {
        return $this->hasOne(Stickers::className(), ['id' => 'sticker_id']);
    }

    public function getUsersMyViews()
    {
        return $this->hasMany(UsersMyViews::className(), ['id_item' => 'id']);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (empty($this->sticker_id)) {
            $this->sticker_id = 0;
        }
        if (is_array($this->dynamic_attributes)) {
            $this->dynamic_attributes = Json::encode($this->dynamic_attributes);
        }
        $this->delta = true;
        if (!$this->isNewRecord) {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return (parent::beforeSave($insert));
    }

    public function afterSave($insert, $values)
    {
        $this->updateImage('_image');
        $this->updateImage('l_image');
        $this->updateImage('p_image');

        SphinxManager::updateIndex(self::deltaIndexName());
        parent::afterSave($insert, $values);
    }

    public function toggleActivityStatus()
    {
        $this->updateAttributes(['is_active' => !!($this->is_active) ? self::STATUS_INACTIVE : self::STATUS_ACTIVE]);
    }

    public function isOwner($user)
    {
        return ($this->user->id == $user->id);
    }

    public function updateImage($column)
    {
        $this->$column = UploadedFile::getInstance($this, $column);
        if ($this->$column AND !empty($this->$column->tempName) AND $this->validate())
        {
            $this->removeImages();
            $this->clearImagesCache();
            if ($column == '_image') {
                $ismain = true;
                $title = $this->$column->name;
            }
            else{
                $ismain = false;
                $title = $this->id . $column;
            }
            $this->attachImage($this->$column->tempName, $ismain, $title, 0, $this->$column->extension);
            $this->clearImagesCache();
            $this->$column->reset();
        }
        if ($this->_deleteimage) {
            $this->removeImages();
            $this->clearImagesCache();
        }
    }
}
