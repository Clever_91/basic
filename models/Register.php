<?php


namespace app\models;

use Yii;
use yii\base\Model;

/**
* @author Sherzod Usmonov
*/
class Register extends Model
{

	public $full_name; 
	public $username; 
	public $email; 
	public $password; 
	public $phone; 

	/**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['full_name', 'username', 'email', 'password', 'phone'], 'required'],
            [['full_name', 'username', 'password', 'phone'], 'string'],
            ['email', 'email']
        ];
    }

    public function save() {
        // something logic is here....
        return true;
    }


}