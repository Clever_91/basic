<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

class Banners extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'foxunion\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public $_image;
    public $_deleteimage;

    public static function tableName()
    {
        return 'tbl_banners';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg png'],
            [['is_visible', '_deleteimage'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['reference', 'image'], 'string', 'max' => 255],
            [['lifetime', 'page'], 'safe'],
            // [['lifetime'], 'date', 'format' => 'Y-m-d'],
            [['place'], 'string', 'max' => 45],
            [['_image'], 'image'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'reference' => 'Ссылка',
            '_image' => 'Изображение',
            'place' => 'Место',
            'page' => 'Страница',
            'lifetime' => 'Время жизни',
            'is_visible' => 'Видимость',
            '_deleteimage' => 'Удалить изображение',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if( is_array ($this->page) ){
                $this->page = json_encode($this->page);
            } 
            return true;
        } else {
            return false;
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removeImages();
            $this->clearImagesCache();
            return true;
        } else {
            return false;
        }
    }

    
    public function getPage() {
        return json_decode($this->page);
    }

    public function getLifetime() {
        return $this->lifetime? date('Y-m-d', $this->lifetime) : '';
    }
    
    public function setLifetime($date) {
        $this->lifetime = $date ? DateTime::createFromFormat('U', $date) : null;
    }

    public function afterSave($insert, $values)
    {
        if( $this->_deleteimage )
        {
            $this->removeImages();
            $this->clearImagesCache();
        }
        $this->imageUpdate();
        parent::afterSave($insert, $values);
    }

    private function imageUpdate()
    {
        $this->_image = UploadedFile::getInstance($this, '_image');
        if ($this->_image AND !empty($this->_image->tempName) AND $this->validate())
        {
            $this->removeImages();
            $this->clearImagesCache();
            $this->attachImage($this->_image->tempName, true, $this->_image->name, 0, $this->_image->extension);
            $this->clearImagesCache();
            $this->_image->reset();
        }
    }

}
